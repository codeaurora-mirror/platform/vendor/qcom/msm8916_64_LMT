# config.mk
#
# Product-specific compile-time definitions.
#

include device/qcom/msm8916_64/BoardConfig.mk

#add suffix variable to uniquely identify the board
TARGET_BOARD_SUFFIX := _64_LMT
